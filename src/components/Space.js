import React, { useContext } from "react";
import { SpaceContext } from "../contexts/SpaceContext";
import styled from "styled-components";

const Space = () => {
  const { currentSpace, setSpace } = useContext(SpaceContext);

  const portalStyle = {
    marginTop: "10px"
  };

  const Headline = styled.h6`
    font-weight: 100;
    font-size: 1.3rem;
    padding: 1rem;
    color: bisque;
    margin: 10px;
  `;

  const SpaceSelector = styled.nav`
    padding-bottom: 1rem;
  `;

  const SpaceInfo = styled.p`
    font-size: 2rem;
  `;

  const Intro = styled.p`
    font-size: 1.3rem;
  `;

  const roomName = styled.p`
    font-size: 1.3rem;
  `;

  const CurrentSpace = styled.span`
    color: pink;
  `;

  return (
    <SpaceSelector>
      <span>
        <Headline style={{marginTop: "30px"}}>Video and voice chat.  No sign-up required.</Headline>
        <Headline>Join a room and get to work. </Headline>
        <SpaceInfo>
          Now in <CurrentSpace>{currentSpace}</CurrentSpace>!
        </SpaceInfo>
      </span>

      <div className="map-container">
        <span className="mapInstructions">
          Click a room to join your team.
        </span>
        <img
          src="map-anyroom.png"
          className="image-map"
          alt="map"
          onClick={() => setSpace("the Lobby")}
        />
        <span className="defaultRoomName">Lobby</span>
        <div
          className="click-zone a"
          data-zone="Living Room"
          onClick={() => setSpace("the Break Room")}
        >
          <span className="roomName">Break Room</span>
        </div>
        <div
          className="click-zone b"
          data-zone="Dining Room"
          onClick={() => setSpace("Team Room 1")}
        >
          <span className="roomName">Team Room 1</span>
        </div>
        <div
          className="click-zone c"
          data-zone="Sunset Patio"
          onClick={() => setSpace("the Conference Room")}
        >
          <span className="roomName">Conference Room</span>
        </div>
        <div
          className="click-zone d"
          data-zone="Portal Zone"
          onClick={() => setSpace("Team Room 2")}
        >
          <span className="roomName" style={portalStyle}>
            Team Room 2
          </span>
        </div>
      </div>
    </SpaceSelector>
  );
};

export default Space;
